# Movie API

In this project we used an Entity Framework Code First workflow to build a movie API with ASP.NET Core in C#. 

The application should comprise of a database in SQL Server through EF Core with a restful api to allow users to manipulate the data.


## Database

#### The database should store information about:

- Characters

- Movies

- Franchises

#### The entities should be governed by the following rules:

- One movie contains many characters, and a character can play in multiple movies

- One movie belongs to one franchise, but a franchise can contain many movies

## Web API

This section gives a brief overview of the endpoints. You may [run the program locally](#run-locally-with-visual-studio) to get an in depth documentation of the system.

### API endpoints


**Characters**

``https
  GET | POST /api/v1/characters
``

``https
  GET | PUT | DELETE /api/v1/characters/${id}
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of character |

*Movies for a character*

``https
  GET | PUT /api/v1/characters/${id}/movies
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of character |

**Movies**

``https
  GET | POST /api/v1/movies
``

``https
  GET | PUT | DELETE /api/v1/movies/${id}
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of movie | 

*Characters in a movie*

``https
  GET | PUT /api/v1/movies/${id}/characters
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of movie | 

**Franchises**

``https
  GET | POST /api/v1/franchises
``

``https
  GET | PUT | DELETE /api/v1/franchises/${id}
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of franchise | 

*Characters in a franchise*

``https
  GET /api/v1/franchises/${id}/characters
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of franchise | 

*Movies in a franchise*

``https
  GET | PUT /api/v1/franchises/${id}/movies
``

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `int` | **Required**. Id of franchise | 

## Run Locally with Visual Studio

Clone the project

```bash
  git clone https://gitlab.com/nreichardt/movieapi
```

Go to the project directory

```bash
  cd movieapi
```

Configure SQL Server connection

```bash
  appsettings.json => DefaultConnection
```

Create database from PMC

```bash
  update-database
```

Start the server

```bash
  F5
```

  
## Contributors

- [@nreichardt](https://gitlab.com/nreichardt)
- [@x-mfh](https://gitlab.com/x-mfh)
- [@vkragh](https://gitlab.com/Vkragh)

  

  
