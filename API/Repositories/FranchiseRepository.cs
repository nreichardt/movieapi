﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories
{
    public class FranchiseRepository : RepositoryBase<Franchise>, IFranchiseRepository
    {

        public FranchiseRepository(MovieDbContext context) : base(context)
        {
        }

        public override async Task<IEnumerable<Franchise>> GetAllAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        public override async Task<Franchise> GetByIdAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
        }

        public async override Task<bool> EntityExistsAsync(int id)
        {
            return await _context.Franchises.AnyAsync(e => e.Id == id);
        }

        /// <summary>
        /// Updates movies in a specific franchise. Must pass all movies that should be in the franchise.
        /// </summary>
        /// <param name="franchiseId">Id of the franchise.</param>
        /// <param name="movieIds">Ids of movies in the franchise.</param>
        /// <returns></returns>
        public async Task UpdateFranchiseMoviesAsync(int franchiseId, int[] movieIds)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(f => f.Movies)
                .Where(f => f.Id == franchiseId)
                .FirstAsync();

            List<Movie> movies = new();

            foreach (int movieId in movieIds)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    throw new KeyNotFoundException();
                movies.Add(movie);
            }

            franchiseToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }


}
