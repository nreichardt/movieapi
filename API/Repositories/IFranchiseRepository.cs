﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models.Domain;

namespace API.Repositories
{
    public interface IFranchiseRepository : IRepositoryBase<Franchise>
    {
        public Task UpdateFranchiseMoviesAsync(int franchiseId, int[] movieIds);
    }

}
