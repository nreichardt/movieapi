﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories
{
    public class CharacterRepository : RepositoryBase<Character>, ICharacterRepository
    {
        public CharacterRepository(MovieDbContext context) : base(context)
        {
        }

        public override async Task<IEnumerable<Character>> GetAllAsync()
        {
            return await _context.Characters.Include(c=>c.Movies).ToListAsync();
        }

        public override async Task<Character> GetByIdAsync(int id)
        {
            return await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.Characters.AnyAsync(e => e.Id == id);
        }

        /// <summary>
        /// Gets all characters for a specific movie.
        /// </summary>
        /// <param name="movieId">Id of the movie.</param>
        /// <returns>A list of characters.</returns>
        public async Task<IEnumerable<Character>> GetAllCharactersByMovieIdAsync(int movieId)
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Select(m => m.Id).Contains(movieId))
                .ToListAsync();
        }

        /// <summary>
        /// Gets all characters for a specific franchise.
        /// </summary>
        /// <param name="franchiseId">Id of the franchise.</param>
        /// <returns>A list of characters.</returns>
        public async Task<IEnumerable<Character>> GetAllCharactersByFranchiseIdAsync(int franchiseId)
        {
            return await _context.Characters.Include(c => c.Movies)
                                .Where(c => c.Movies.Select(m => m.FranchiseId).Contains(franchiseId))
                                .ToListAsync();
        }

        /// <summary>
        /// Updates movies in a specific character. Must pass all movies that should be in the character.
        /// </summary>
        /// <param name="characterId">Id of the character.</param>
        /// <param name="movieIds">Ids of movies in the character.</param>
        /// <returns></returns>
        public async Task UpdateCharacterMoviesAsync(int characterId, int[] movieIds)
        {
            Character charToUpdateMovies = await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Id == characterId)
                .FirstAsync();

            List<Movie> movies = new();

            foreach (int movieId in movieIds)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    throw new KeyNotFoundException();
                movies.Add(movie);
            }

            charToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }
    }

}
