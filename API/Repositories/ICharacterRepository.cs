﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models.Domain;

namespace API.Repositories
{
    public interface ICharacterRepository : IRepositoryBase<Character>
    {
        public Task<IEnumerable<Character>> GetAllCharactersByMovieIdAsync(int movieId);
        public Task<IEnumerable<Character>> GetAllCharactersByFranchiseIdAsync(int franchiseId);
        public Task UpdateCharacterMoviesAsync(int characterId, int[] movieIds);
    }
}
