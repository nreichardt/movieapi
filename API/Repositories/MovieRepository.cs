﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using API.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace API.Repositories
{
    public class MovieRepository : RepositoryBase<Movie>, IMovieRepository
    {
        public MovieRepository(MovieDbContext context) : base(context)
        {
        }

        public override async Task<IEnumerable<Movie>> GetAllAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .ToListAsync();
        }

        public override async Task<Movie> GetByIdAsync(int id)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .FirstOrDefaultAsync(m => m.Id == id);
        }

        /// <summary>
        /// Gets all movies for a specific franchise.
        /// </summary>
        /// <param name="franchiseId">Id of the franchise.</param>
        /// <returns>A list of movies.</returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesByFranchiseIdAsync(int franchiseId)
        {
            return await _context.Movies.Include(m => m.Characters)
                .Where(m => m.FranchiseId == franchiseId)
                .ToListAsync();
        }

        /// <summary>
        /// Updates characters in a specific movie. Must pass all characters that should be in the movie.
        /// </summary>
        /// <param name="movieId">Id of the movie.</param>
        /// <param name="characterIds">Ids of characters in the movie.</param>
        /// <returns></returns>
        public async Task UpdateMovieCharactersAsync(int movieId, int[] characterIds)
        {
            Movie movieToUpdateChars = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Id == movieId)
                .FirstAsync();

            List<Character> chars = new();

            foreach (int characterId in characterIds)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                    throw new KeyNotFoundException();
                chars.Add(character);
            }

            movieToUpdateChars.Characters = chars;
            await _context.SaveChangesAsync();
        }

        public override async Task<bool> EntityExistsAsync(int id)
        {
            return await _context.Movies.AnyAsync(e => e.Id == id);
        }

        /// <summary>
        /// Gets all movies for a specific character.
        /// </summary>
        /// <param name="characterId">Id of the character.</param>
        /// <returns>A list of movies.</returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesByCharacterIdAsync(int characterId)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.Characters.Select(c => c.Id).Contains(characterId))
                .ToListAsync();
        }
    }
}
