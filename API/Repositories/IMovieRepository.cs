﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models.Domain;

namespace API.Repositories
{
    public interface IMovieRepository : IRepositoryBase<Movie>
    {
        public Task<IEnumerable<Movie>> GetAllMoviesByFranchiseIdAsync(int franchiseId);
        public Task<IEnumerable<Movie>> GetAllMoviesByCharacterIdAsync(int characterId);
        public Task UpdateMovieCharactersAsync(int movieId, int[] characterIds);
    }

}
