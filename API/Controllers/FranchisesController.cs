﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using API.Models.DTO.Movie;
using System.Net.Mime;
using AutoMapper;
using API.Models.DTO.Franchise;
using API.Repositories;
using API.Models.DTO.Character;

namespace API.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseRepository _franchiseRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;


        public FranchisesController(
            IFranchiseRepository franchiseRepository, 
            IMovieRepository movieRepository, 
            ICharacterRepository characterRepository,
            IMapper mapper)
        {
            _franchiseRepository = franchiseRepository;
            _movieRepository = movieRepository;
            _characterRepository = characterRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the franchises.
        /// </summary>
        /// <returns>A list of franchises.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseRepository.GetAllAsync());
        }

        /// <summary>
        /// Gets a specific franchise by their id.
        /// </summary>
        /// <param name="id">Id of the franchise.</param>
        /// <returns>A single franchise.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseRepository.GetByIdAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Gets all movies for a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise.</param>
        /// <returns>A list of movies.</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesByFranchise(int id)
        {
            return _mapper.Map<List<MovieReadDTO>>(
                await _movieRepository.GetAllMoviesByFranchiseIdAsync(id));
        }

        /// <summary>
        /// Gets all characters for a specific franchise.
        /// </summary>
        /// <param name="id">Id of the franchise.</param>
        /// <returns>A list of characters.</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersByFranchise(int id)
        {
            return _mapper.Map<List<CharacterReadDTO>>(
                await _characterRepository.GetAllCharactersByFranchiseIdAsync(id));
        }

        /// <summary>
        /// Adds a new franchise to the database.
        /// </summary>
        /// <param name="dtoFranchise">Id of the franchise.</param>
        /// <returns>The newly created franchise.</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseRepository.AddAsync(domainFranchise);

            return CreatedAtAction("GetFranchise", new { id = domainFranchise.Id }, _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Updates a franchise. Must pass a full franchise object and Id in route.
        /// </summary>
        /// <param name="id">Id of the franchise.</param>
        /// <param name="dtoFranchise">Edited franchise object.</param>
        /// <returns>No content.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }

            if(!await _franchiseRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseRepository.UpdateAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Updates movies in a specific franchise. Must pass all movies that should be in the franchise.
        /// </summary>
        /// <param name="id">Id of the franchise.</param>
        /// <param name="movieIds">Ids of movies in the franchise.</param>
        /// <returns>No content.</returns>
        [HttpPut("{id}/movies")]
        public async Task<ActionResult> PutFranchiseMovies(int id, int[] movieIds)
        {
            if (!await _franchiseRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseRepository.UpdateFranchiseMoviesAsync(id, movieIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie.");
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a franchise from the database.
        /// </summary>
        /// <param name="id">Id of the franchise.</param>
        /// <returns>No content.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!await _franchiseRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }
            await _franchiseRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
