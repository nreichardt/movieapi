﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using API.Repositories;
using API.Models.DTO.Character;
using AutoMapper;
using System.Net.Mime;
using API.Models.DTO.Movie;

namespace API.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;

        public CharactersController(
            ICharacterRepository characterRepository,
            IMovieRepository movieRepository,
            IMapper mapper)
        {
            _characterRepository = characterRepository;
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the characters.
        /// </summary>
        /// <returns>A list of characters.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterRepository.GetAllAsync());
        }

        /// <summary>
        /// Get a specific character by their id.
        /// </summary>
        /// <param name="id">Id of the character.</param>
        /// <returns>A single character.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterRepository.GetByIdAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Gets all movies for a specific character.
        /// </summary>
        /// <param name="id">Id of the character.</param>
        /// <returns>A list of movies.</returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesByCharacter(int id)
        {
            return _mapper.Map<List<MovieReadDTO>>(
                await _movieRepository.GetAllMoviesByCharacterIdAsync(id));
        }

        /// <summary>
        /// Adds a new character to the database.
        /// </summary>
        /// <param name="dtoCharacter">The character to add.</param>
        /// <returns>The newly created character.</returns>
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            var domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterRepository.AddAsync(domainCharacter);

            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.Id },
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Updates a character. Must pass a full character object and Id in route.
        /// </summary>
        /// <param name="id">Id of the character.</param>
        /// <param name="dtoCharacter">Edited character object.</param>
        /// <returns>No content.</returns>

        [HttpPut("{id}")]
        public async Task<ActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            if (!await _characterRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterRepository.UpdateAsync(domainCharacter);

            return NoContent();
        }

        /// <summary>
        /// Updates movies in a specific character. Must pass all movies that should be in the character.
        /// </summary>
        /// <param name="id">Id of the character.</param>
        /// <param name="movieIds">Ids of movies in the character.</param>
        /// <returns>No content.</returns>
        [HttpPut("{id}/movies")]
        public async Task<ActionResult> PutCharacterMovies(int id, int[] movieIds)
        {
            if (!await _characterRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _characterRepository.UpdateCharacterMoviesAsync(id, movieIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character.");
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a character from the database.
        /// </summary>
        /// <param name="id">Id of the character.</param>
        /// <returns>No content.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            if (!await _characterRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            await _characterRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
