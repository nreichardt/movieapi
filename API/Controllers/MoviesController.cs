﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Models.Domain;
using AutoMapper;
using API.Models.DTO.Movie;
using API.Repositories;
using API.Models.DTO.Character;
using System.Net.Mime;

namespace API.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;

        public MoviesController(
            IMovieRepository movieRepository, 
            ICharacterRepository characterRepository, 
            IMapper mapper)
        {
            _characterRepository = characterRepository;
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all the movies.
        /// </summary>
        /// <returns>A list of movies.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieRepository.GetAllAsync());
        }

        /// <summary>
        /// Gets a specific movie by their id.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <returns>A single movie.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieRepository.GetByIdAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Gets all characters for a specific movie.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <returns>A list of characters.</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersByMovie(int id)
        {
            return _mapper.Map<List<CharacterReadDTO>>(
                await _characterRepository.GetAllCharactersByMovieIdAsync(id));
        }

        /// <summary>
        /// Adds a new movie to the database.
        /// </summary>
        /// <param name="dtoMovie">The movie to add.</param>
        /// <returns>The newly created movie.</returns>
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO dtoMovie)
        {
            var domainMovie = _mapper.Map<Movie>(dtoMovie);

            await _movieRepository.AddAsync(domainMovie);

            return CreatedAtAction("GetMovie", 
                new { id = domainMovie.Id },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Updates a movie. Must pass a full movie object and Id in route.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <param name="dtoMovie">Edited movie object.</param>
        /// <returns>No content.</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!await _movieRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieRepository.UpdateAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Updates characters in a specific movie. Must pass all characters that should be in the movie.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <param name="characterIds">Ids of characters in the movie.</param>
        /// <returns>No content.</returns>
        [HttpPut("{id}/characters")]
        public async Task<ActionResult> PutMovieCharacters(int id, int[] characterIds)
        {
            if (!await _movieRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            try
            {
                await _movieRepository.UpdateMovieCharactersAsync(id, characterIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character.");
            }

            return NoContent();
        }

        /// <summary>
        /// Deletes a movie from the database.
        /// </summary>
        /// <param name="id">Id of the movie.</param>
        /// <returns>No content.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            if (!await _movieRepository.EntityExistsAsync(id))
            {
                return NotFound();
            }

            await _movieRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}
