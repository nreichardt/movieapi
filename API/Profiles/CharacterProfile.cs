﻿using API.Models.Domain;
using API.Models.DTO.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Id).ToList()))
                .ReverseMap();

            CreateMap<Character, CharacterCreateDTO>()
                    .ReverseMap();

            CreateMap<CharacterEditDTO, Character>()
                    .ReverseMap();
        }
    }
}
