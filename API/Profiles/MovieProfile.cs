﻿using API.Models.Domain;
using API.Models.DTO.Movie;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(m => m.Id).ToList()))
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

            CreateMap<MovieCreateDTO, Movie>()
                .ForMember(m => m.FranchiseId, opt => opt
                .MapFrom(mdto => mdto.Franchise))
                .ForMember(m => m.Franchise, opt => opt.Ignore())
                .ReverseMap();

            CreateMap<MovieEditDTO, Movie>()
                .ForMember(m => m.FranchiseId, opt => opt
                .MapFrom(mdto => mdto.Franchise))
                .ForMember(m => m.Franchise, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
