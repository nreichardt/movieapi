﻿using API.Models.Domain;
using API.Models.DTO.Franchise;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m=>m.Id).ToList()))
                .ReverseMap();

            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();

            CreateMap<Franchise, FranchiseEditDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToList()))
                .ReverseMap();
        }
    }
}
