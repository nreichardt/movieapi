﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.Domain
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string MovieName { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [MaxLength(4)]
        public int ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        [MaxLength(100)]
        public string PictureURL { get; set; }
        [MaxLength(100)]
        public string TrailerURL { get; set; }

        public ICollection<Character> Characters { get; set; }

        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
