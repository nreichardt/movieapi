﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.Domain
{
    public class Franchise
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string FranchiseName { get; set; }
        public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
