﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace API.Models
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Movie> Movies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var franchises = new[]
            {
                new Franchise {Id = 1, FranchiseName = "Frans1", Description = "Making movies"},
                new Franchise {Id = 2, FranchiseName = "Frans2", Description = "Making movies aswell"}
            };


            var characters = new[]
            {
                new Character {Id = 1, Fullname = "Mikkel Hammer", Alias = "The Young One", Gender = "Unknown"},
                new Character {Id = 2, Fullname = "Nikolaj Reichardt", Alias = "The Wise One", Gender = "Male"},
                new Character {Id = 3, Fullname = "Valdemar Simp", Alias = "The Simp", Gender = "Simp"}
            };

            var movies = new[]
            {
                new Movie {Id = 1, MovieName = "Experis Academy", FranchiseId = 1},
                new Movie {Id = 2, MovieName = "Experis Academy 2", FranchiseId = 1},
                new Movie {Id = 3, MovieName = "Experis Academy 3", FranchiseId = 2},
                new Movie {Id = 4, MovieName = "Dewald's Nightmares"},
                new Movie {Id = 5, MovieName = "Robin Hood"},
            };


            modelBuilder.Entity<Franchise>().HasData(franchises);

            modelBuilder.Entity<Character>().HasData(characters);

            modelBuilder.Entity<Movie>().HasData(movies);
            modelBuilder.Entity<Movie>()
                .HasOne(m => m.Franchise)
                .WithMany(f => f.Movies)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 1, CharacterId = 3 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 3 }
                        );
                    });
        }

    }
}
