﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.DTO.Character
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string PictureURL { get; set; }
        public List<int> Movies { get; set; }
    }
}
