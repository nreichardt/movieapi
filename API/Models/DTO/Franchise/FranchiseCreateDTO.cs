﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.DTO.Franchise
{
    public class FranchiseCreateDTO
    {
        public string FranchiseName { get; set; }
        public string Description { get; set; }
    }
}
