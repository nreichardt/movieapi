﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models.DTO.Movie
{
    public class MovieCreateDTO
    {
        public string MovieName { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; } = DateTime.Now.Year;
        public string Director { get; set; }
        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }
        public int? Franchise { get; set; }
    }
}
